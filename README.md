#### Performance testing

1. sbt run MainApp.scala
2. sbt gatling: testOnly load.ThrottlingOnScript
3. sbt gatling: testOnly load.ThrottlingOffScript

Results will be generated to /target/gatling/
