package load

import scala.concurrent.duration._

import app.misc.FakeDataCreator.createMockForDb
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef._

class ThrottlingOnScript extends Simulation with Common  {
  val httpConf = http.baseURL("http://localhost:8080")

  val scn = scenario("ThrottlingSimulation")
    .feed(tokenFeed.random)
    .exec(requestWithThrottling)

  setUp(scn.inject(constantUsersPerSec(100).during(1 minute))
    .protocols(httpConf))
}

class ThrottlingOffScript extends Simulation with Common {
  val httpConf = http.baseURL("http://localhost:8080")

  val scn = scenario("ThrottlingSimulation")
    .feed(tokenFeed.random)
    .exec(requestWithoutThrottling)

  setUp(scn.inject(constantUsersPerSec(100).during(1 minute))
    .protocols(httpConf))
}

trait Common {
  val tokenFeed: Array[Map[String, String]] = {
    createMockForDb()
      .map { case (token, _) =>
        Map("Authorization" -> token)
      } ++ Array.fill(20)(Map("Authorization" -> "unauthorized"))
  }

  val requestWithThrottling = exec(
    http("rps_test")
      .get("/test")
      .header("Authorization", "${Authorization}")
      .header("ThrottlingTurnedOn", "on")
      .check(status.is(200))
  )

  val requestWithoutThrottling = exec(
    http("rps_test")
      .get("/test")
      .header("Authorization", "${Authorization}")
      .check(status.is(200))
  )
}
