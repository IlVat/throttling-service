package app.services

import java.time.Instant

import scala.concurrent.Future

import akka.http.scaladsl.testkit.ScalatestRouteTest

import app.cache.{SlaCache, TokenCache}
import app.models.{CachedSla, Sla}
import org.mockito.ArgumentMatchers.any
import org.mockito.Mockito._
import org.scalatest.concurrent.Eventually
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{BeforeAndAfterEach, FreeSpec, Matchers}
import scala.concurrent.duration._

class ThrottlingServiceTest
  extends FreeSpec
    with MockitoSugar
    with Matchers
    with ScalatestRouteTest
    with BeforeAndAfterEach
    with Eventually
{
  implicit val ec = system.dispatcher
  
  val slaService = mock[SlaService]
  val tokenCache = mock[TokenCache]
  val slaCache = mock[SlaCache]
  
  override def afterEach(): Unit = {
    super.afterEach()
    reset(slaService, tokenCache, slaCache)
  }
  
  def throttlingService(graceRps: Int) = new ThrottlingService(slaService, slaCache, tokenCache, graceRps)
  
  "ThrottlingService" - {
    "isRequestAllowed should return true" - {
      val graceRps = 10
      val service = throttlingService(graceRps)
      "if user is unauthorized and rpsCounter > 0" in {
        val isRequestAllowed = service.isRequestAllowed(None)
        service.getRpsCounter.get() shouldBe graceRps - 1
        isRequestAllowed shouldBe true
      }
      
      "if user is authorized and rpsCounter > 0" in {
        val sla = CachedSla(10, 10, Instant.now())
        val updatedSla = sla.copy(rpsCount = sla.rpsCount - 1)
        when(tokenCache.get(any())).thenReturn(Some("user"))
        when(slaCache.get(any())).thenReturn(Some(sla))
        when(slaCache.update(any(), any())).thenReturn(Some(sla))
        
        val isRequestAllowed = service.isRequestAllowed(Some("token"))
        verify(tokenCache).get("token")
        verify(slaCache).get("user")
        verify(slaCache).update("user", updatedSla)
        verifyNoMoreInteractions(tokenCache)
        verifyNoMoreInteractions(slaCache)
        isRequestAllowed shouldBe true
      }
    }
    
    "isRequestAllowed should return false" - {
      val graceRps = 0
      val service = throttlingService(graceRps)
      
      "if user is unauthorized and rpsCounter <= 0" in {
        val isRequestAllowed = service.isRequestAllowed(None)
        service.getRpsCounter.get() shouldBe graceRps
        isRequestAllowed shouldBe false
      }
      
      "if user is authorized and rpsCounter == 0" in {
        val sla = CachedSla(10, 0, Instant.now())
        
        when(tokenCache.get(any())).thenReturn(Some("user"))
        when(slaCache.get(any())).thenReturn(Some(sla))
        
        val isRequestAllowed = service.isRequestAllowed(Some("token"))
        
        verify(tokenCache).get("token")
        verify(slaCache).get("user")
        verifyNoMoreInteractions(tokenCache)
        verifyNoMoreInteractions(slaCache)
        
        isRequestAllowed shouldBe false
      }
    }
    
    "isRequestAllowed should return false" - {
      "and call slaService if sla for the user hasn't been cached yet" in {
        val graceRps = 1
        val service = throttlingService(graceRps)
        
        val sla = Sla("user", 10)
        val cachedSla = CachedSla(10, 10, Instant.now())
        
        when(tokenCache.get(any())).thenReturn(None)
        when(tokenCache.update(any(), any())).thenReturn(Some("user"))
        when(tokenCache.putIfAbsent(any(), any())).thenReturn(Some(""))
        when(slaCache.putIfAbsent(any(), any())).thenReturn(Some(cachedSla))
        when(slaService.getSlaByToken(any())).thenReturn(Future.successful(Some(sla)))
        
        val isRequestAllowed = service.isRequestAllowed(Some("token"))
        verify(tokenCache, times(2)).get("token")
        verify(tokenCache).putIfAbsent("token", "")
        verify(slaService).getSlaByToken("token")
        eventually(timeout(100.millis), interval(10.millis)) {
          verify(tokenCache).update("token", "user")
        }
        verifyNoMoreInteractions(tokenCache)
        verifyNoMoreInteractions(slaService)
        
        service.getRpsCounter.get() shouldBe graceRps - 1
        isRequestAllowed shouldBe true
      }
      
      "but not call slaService if request has a token, but slaService has not returned any info yet" in {
        val graceRps = 1
        val service = throttlingService(graceRps)
        
        when(tokenCache.get(any())).thenReturn(Some(""))
        when(slaCache.get(any())).thenReturn(None)
        
        val isRequestAllowed = service.isRequestAllowed(Some("token"))
        verify(tokenCache, times(2)).get("token")
        verifyNoMoreInteractions(tokenCache)
        verifyZeroInteractions(slaService)
        
        service.getRpsCounter.get() shouldBe graceRps - 1
        isRequestAllowed shouldBe true
      }
    }
  }
}
