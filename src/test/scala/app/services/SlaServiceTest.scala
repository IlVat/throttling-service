package app.services

import scala.concurrent.Future

import app.db.FakeSlaDatabase
import app.models.Sla
import org.mockito.Mockito.{verify, when}
import org.mockito.ArgumentMatchers.any
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{AsyncFreeSpec, Matchers}

class SlaServiceTest extends AsyncFreeSpec with MockitoSugar with Matchers {
  val db = mock[FakeSlaDatabase]
  val slaService = new SlaService(db)
  
  "SlaService" - {
    "getSla should return Sla" in {
      val slaOpt = Some(Sla("user", 10))
      when(db.getSla(any())).thenReturn(Future.successful(slaOpt))
      
      slaService.getSlaByToken("token").map { result =>
        verify(db).getSla("token")
        result shouldBe slaOpt
      }
    }
  }
}
