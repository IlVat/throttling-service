package app.routers

import akka.http.scaladsl.testkit.ScalatestRouteTest

import org.scalatest.{Matchers, WordSpec}

class RouteApiTest extends WordSpec with Matchers with ScalatestRouteTest {
  "RouteApi" should {
    "return Done" in {
      Get("/test") ~> RouteApi.route ~> check {
        responseAs[String] shouldEqual "Done"
      }
    }
  }
}
