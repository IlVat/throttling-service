package app.misc

import app.models.Sla

object FakeDataCreator {
  def createFakeData(start: Int = 1, end: Int = 100, tokenName: String = "token", rps: Int = 5): Array[(String, Sla)] = {
    (start to end).map { t =>
      s"$tokenName-$t" -> Sla(user = s"user-$t", rps = rps)
    }.toArray
  }

  //100 unique users with 130 unique tokens will be created
  def createMockForDb(rps: Int = 5) = {
    createFakeData(tokenName = "first_token", rps = rps) ++
      createFakeData(end = 30, tokenName = "second_token", rps = rps)
  }
}
