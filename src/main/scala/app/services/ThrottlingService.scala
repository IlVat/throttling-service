package app.services

import java.time.Instant
import java.util.concurrent.atomic.AtomicInteger

import scala.concurrent.{ExecutionContext, Future}

import akka.actor.{ActorSystem, Cancellable}

import app.cache.{SlaCache, TokenCache}
import app.models.{CachedSla, Sla}
import scala.concurrent.duration._
import scala.util.control.NonFatal

trait ThrottlingServiceLike {
  val graceRps: Int
  val slaService: SlaServiceLike
  
  def isRequestAllowed(token: Option[String]): Boolean
}

class ThrottlingService(sla: SlaServiceLike,
                        slaCache: SlaCache,
                        tokenCache: TokenCache,
                        defaultRps: Int)
                       (implicit sys: ActorSystem, ec: ExecutionContext)
  extends ThrottlingServiceLike
{
  val graceRps: Int = defaultRps
  val slaService: SlaServiceLike = sla
  private val rpsCounter = new AtomicInteger(graceRps)
  
  def isRequestAllowed(token: Option[String]): Boolean = token match {
    case None if rpsCounter.get() > 0 =>
      rpsCounter.decrementAndGet()
      true
    case None => false
    case Some(t) =>
      val isSlaCacheUpdated =
        for {
          user <- tokenCache.get(t)
          sla <- slaCache.get(user)
        } yield {
          if (sla.rpsCount > 0) {
            slaCache.update(user, sla.copy(rpsCount = sla.rps - 1))
            true
          } else false
        }
      
      isSlaCacheUpdated.getOrElse {
        //call slaService only if token is absent in userCache
        tokenCache.get(t).getOrElse {
          tokenCache.putIfAbsent(t, "")
          getSlaAndUpdateCache(t)
            .recover {
              case NonFatal(_) => tokenCache.remove(t)
            }
        }

        if(rpsCounter.get() > 0) {
          rpsCounter.decrementAndGet()
          true
        } else false
      }
  }
  
  def runRpsCounterUpdating: Cancellable = updateRpsCounter
  
  def getRpsCounter = rpsCounter
  
  private def getSlaAndUpdateCache(t: String): Future[Option[CachedSla]] = {
    slaService
      .getSlaByToken(t)
      .map(_.flatMap(addToCache(t, _)))
  }
  
  private def addToCache(token: String, sla: Sla): Option[CachedSla] = {
    tokenCache.update(token, sla.user)
    slaCache.putIfAbsent(sla.user, CachedSla(rps = sla.rps, rpsCount = sla.rps, Instant.now()))
  }
  
  private def updateRpsCounter = {
    sys.scheduler.schedule(0.second, 1.second) {
      rpsCounter.set(graceRps)
    }
  }
}
