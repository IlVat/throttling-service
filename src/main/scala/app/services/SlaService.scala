package app.services

import scala.concurrent.{ExecutionContext, Future}

import app.db.FakeSlaDatabase
import app.models.Sla

trait SlaServiceLike {
  //token could be absent in database, so I use Option
  def getSlaByToken(token:String):Future[Option[Sla]]
}

class SlaService(db: FakeSlaDatabase)(implicit ec: ExecutionContext) extends SlaServiceLike {
  override def getSlaByToken(token: String): Future[Option[Sla]] = {
    db.getSla(token)
  }
}