package app

import java.util.concurrent.Executors

import scala.concurrent.ExecutionContext
import scala.util.Try

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer

import app.cache.{SlaCache, SlaCacheUpdatingTask, TokenCache}
import app.db.FakeSlaDatabase
import app.http.ThrottlingDirectives
import app.misc.FakeDataCreator._
import app.routers.RouteApi
import app.services.{SlaService, ThrottlingService}
import com.typesafe.config.ConfigFactory

object MainApp extends App with ThrottlingDirectives {
  val config = ConfigFactory.load()
  val graceRps: Int = Try(config.getInt("rps.default")).getOrElse(100)
  
  implicit val system = ActorSystem("throttling-service-system")
  implicit val materializer = ActorMaterializer()
  implicit val ec = system.dispatcher
  
  val slaCache = new SlaCache()
  val userCache = new TokenCache()
  
  val db = {
    implicit val ec = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(10)) //just for task
    new FakeSlaDatabase()
  }

  println("==Inserting records to database==")

  createMockForDb(rps = 2)
    .foreach{
      case (t, sla) =>
        db.insert(t, sla)
    }

  println("==The records was inserted==")

  val slaService = new SlaService(db)
  
  val throttlingService = new ThrottlingService(slaService, slaCache, userCache, graceRps)
  
  val slaCacheUpdatingTask = new SlaCacheUpdatingTask(slaCache)
  
  val route = throttleRoutes(RouteApi.route)
  
  slaCacheUpdatingTask.runUpdating
  throttlingService.runRpsCounterUpdating
  
  val  bindingFuture = Http().bindAndHandle(route, "localhost", 8080)
  
  println(s"Server online at http://localhost:8080/")
}
