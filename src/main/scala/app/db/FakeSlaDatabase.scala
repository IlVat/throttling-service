package app.db

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

import app.models.Sla

class FakeSlaDatabase()(implicit ec: ExecutionContext) {
  private val slaDB = collection.mutable.Map.empty[String, Sla]
  
  //could be either type here: Future[Either[Error, Sla]]
  def getSla(token: String): Future[Option[Sla]] = {
    Future {
      Thread.sleep(250)
      slaDB.get(token)
    }
  }

  def insert(token: String, sla: Sla) = slaDB += (token -> sla)
}
