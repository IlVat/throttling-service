package app.models

import java.time.Instant

case class Sla(user: String, rps: Int)

case class CachedSla(
  rps: Int,
  rpsCount: Int,
  startTime: Instant
)
