package app.http

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.{Directive, Directive1, Route, StandardRoute}

import app.services.ThrottlingServiceLike
import akka.http.scaladsl.server.Directives._


trait ThrottlingDirectives {
  private val authHeader = "Authorization"
  private val throttleHeader = "ThrottlingTurnedOn"
  val throttlingService: ThrottlingServiceLike

  def throttleRoutes(routes: Route): Route = throttle { _ => routes }
  
  private def throttle: Directive1[StandardRoute] = {
    optionalHeaderValueByName(throttleHeader).flatMap { throttling =>
      optionalHeaderValueByName(authHeader).map { token =>
        if (throttling.isEmpty || throttlingService.isRequestAllowed(token))
          complete(StatusCodes.OK)
        else {
          val tooManyRequests = StatusCodes.TooManyRequests
          println(s"Server respond: ${tooManyRequests.defaultMessage}")
          complete(tooManyRequests)
        }
      }
    }
  }
}
