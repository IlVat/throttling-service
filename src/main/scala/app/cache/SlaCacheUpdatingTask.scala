package app.cache

import java.time.Instant

import akka.actor.{ActorSystem, Cancellable}
import scala.concurrent.duration._

import app.common.User
import app.models.CachedSla

class SlaCacheUpdatingTask(cache: SlaCache)(implicit sys: ActorSystem) {
  import SlaCacheUpdatingTask._
  
  implicit val ec = sys.dispatcher
  
  def runUpdating: Cancellable = updateCache()
  
  private def updateCache() = {
    sys.scheduler.schedule(0.millis, 100.millis) {
      cache.getCache.foreach {
        case (key, sla) =>
          updateCachedSla(key, sla)
      }
    }
  }
  
  private def updateCachedSla(key: User, sla: CachedSla) = {
    val currentTime = Instant.now()
    if (currentTime.minusSeconds(1).isAfter(sla.startTime)) {
      cache.update(key, sla.copy(rpsCount = sla.rps, startTime = currentTime))
    }
    else if (sla.rpsCount <= 0)
      cache.update(key, sla.copy(rps = sla.rps * Factor))
    else
      sla
  }
}

object SlaCacheUpdatingTask {
  private val Factor = 10 / 100
}
