package app.cache

import scala.collection.concurrent.TrieMap

trait Cache[K,V] {
  protected val cache: TrieMap[K, V]
  
  def putIfAbsent(key: K, value: V): Option[V] = cache.putIfAbsent(key, value)
  def get(key: K): Option[V] = cache.get(key)
  def update(key: K, value: V): Option[V] = cache.replace(key, value)
  def remove(key: K): Option[V] = cache.remove(key)
}
