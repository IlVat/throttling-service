package app.cache

import scala.collection.concurrent.TrieMap

import app.common.{Token, User}

class TokenCache extends Cache[Token, User] {
  protected val cache = TrieMap.empty[Token, User]
}
