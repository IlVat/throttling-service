package app.cache

import scala.collection.concurrent.TrieMap

import app.common.User
import app.models.CachedSla

class SlaCache extends Cache[User, CachedSla] {
  protected val cache = TrieMap.empty[User, CachedSla]
  
  def getCache: TrieMap[User, CachedSla] = cache
}
