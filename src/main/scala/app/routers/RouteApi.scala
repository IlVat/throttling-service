package app.routers

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route

trait RouteApi {
  def route: Route = path("test") {
    get {
      complete("Done")
    }
  }
}
object RouteApi extends RouteApi
