name := "throttling-service"

version := "0.1"

scalaVersion := "2.12.5"

enablePlugins(GatlingPlugin)

libraryDependencies := Seq(
  "com.typesafe.akka"     %% "akka-http"                  % "10.1.1",
  "com.typesafe.akka"     %% "akka-stream"                % "2.5.11",
  "org.scalatest"         %% "scalatest"                  % "3.0.5"       % Test,
  "com.typesafe.akka"     %% "akka-http-testkit"          % "10.1.1"      % Test,
  "org.mockito"            % "mockito-core"               % "2.18.0"      % Test,
  "io.gatling.highcharts"  % "gatling-charts-highcharts"  % "2.3.0"       % Test,
  "io.gatling"             % "gatling-test-framework"     % "2.3.0"       % Test
)
